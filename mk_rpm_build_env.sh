#
#    Copyright (C) 2013, 2017  Milan Kupcevic
#
#    You can redistribute and/or modify this program under the terms of
#    the GNU General Public License version 3, or any later version as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    GPLv3+
#

set -e

packages="
rpmdevtools
redhat-rpm-config
make
gcc
"

for package in $packages; do
  rpm -q $package || {
    [ "$USER" == "root" ] && {
      yum -y install $package
    }
  }
done

[ -e $HOME/rpmbuild ] || {
  mkdir -p $HOME/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
}

[ -e $HOME/.rpmmacros ] || {
  echo "%vendor Vendor Name" >> $HOME/.rpmmacros
  echo "%packager First Last <email@address.tld>" >> $HOME/.rpmmacros
  echo "%_gpg_name First Last <email@address.tld>" >> $HOME/.rpmmacros
  echo "%_topdir %(echo $HOME)/rpmbuild/" >> $HOME/.rpmmacros
}

cat $HOME/.rpmmacros

